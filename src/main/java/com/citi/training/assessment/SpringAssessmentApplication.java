package com.citi.training.assessment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
* <h1> The Beautiful Assessment </h1>
*
* @author  Charles Mountain
* @version 0.0.1
* @since   2019-07-19
*/
@SpringBootApplication
public class SpringAssessmentApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(SpringAssessmentApplication.class);
	
	public SpringAssessmentApplication() {
		logger.debug("Roasting Beans");
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringAssessmentApplication.class, args);
	}

}
