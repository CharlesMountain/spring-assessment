package com.citi.training.assessment.dao;

import java.util.List;

import com.citi.training.assessment.model.Trade;

/**
* <h2> The TradeDao Interface </h2>
* 
* Responsible for allowing multiple
* types of Dao to interact with service and controller
*/
public interface TradeDao {

	List<Trade> findAll(); 
	Trade findById(int id);
	int create(Trade product);
	void deleteById(int id);
}
