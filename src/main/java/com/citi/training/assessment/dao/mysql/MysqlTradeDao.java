package com.citi.training.assessment.dao.mysql;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.exception.ResourceNotFoundException;
import com.citi.training.assessment.model.Trade;

/**
* <h2> The MysqlTradeDao Class </h2>
* 
* Responsible for implementing the interface function
* using a Mysql database via sql queries
*/
@Component
public class MysqlTradeDao implements TradeDao{
	
	@Autowired
	JdbcTemplate tpl;
	
	private static final Logger logger = LoggerFactory.getLogger(MysqlTradeDao.class);
	
	public MysqlTradeDao() {
		logger.debug("MysqlTradeDao Initialized");
	}
	
	public List<Trade> findAll(){
		logger.debug("Querying database for all rows");
		return tpl.query("select id, stock, price, volume from trade", new TradeMapper());
	}
	
	public Trade findById(int id) {
		logger.debug("Querying database for given id");
        List<Trade> trades = this.tpl.query(
                "select id, stock, price, volume from trade where id = ?",
                new Object[]{id},
                new TradeMapper()
        );
        if(trades.size() <= 0) {
        	throw new ResourceNotFoundException();
        }
        return trades.get(0);
	}
	
	public int create(Trade trade) {
		logger.debug("Querying database to add trade");
		KeyHolder keyHolder = new GeneratedKeyHolder();
	    this.tpl.update(
	    		new PreparedStatementCreator() {
	                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	    				PreparedStatement ps =
	                			connection.prepareStatement("insert into trade (stock, price, volume) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
	                    ps.setString(1, trade.getStock());
	                    ps.setDouble(2, trade.getPrice());
	                    ps.setInt(3, trade.getVolume());
	                    return ps;
	                }
	            },
	            keyHolder);
	        return keyHolder.getKey().intValue();
	}
	
	public void deleteById(int id) {
		logger.debug("Querying database to delete id", id);
		int del = this.tpl.update("delete from trade where id=?",id);
		if(del == 0) {
			logger.debug("Id not found");
			throw new ResourceNotFoundException();
		}
    }

	private static final class TradeMapper implements RowMapper<Trade>{
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException{
			return new Trade(rs.getInt("id"), rs.getString("stock"), rs.getDouble("price"), rs.getInt("volume"));
		}
	}
}
