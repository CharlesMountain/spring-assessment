package com.citi.training.assessment.exception;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.citi.training.assessment.exception.ResourceNotFoundException;

/**
* <h2> The DefaultExceptionHandler Exception </h2>
* 
* Responsible for handling all generic exceptions
*/
@ControllerAdvice
@Priority(20)
public class DefaultExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    public ResponseEntity<Object> productNotFoundExceptionHandler(
            HttpServletRequest request, ResourceNotFoundException ex) {
        logger.debug(ex.toString());
        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
                                    HttpStatus.NOT_FOUND);
    }
}