package com.citi.training.assessment.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
* <h2> The ResourceNotFoundException Exception </h2>
*
* Outputs 404 exceptions when called
*/
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Id Not Found")
public class ResourceNotFoundException extends RuntimeException {
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceNotFoundException.class);
	
	public ResourceNotFoundException() {
		logger.debug("Returning 404 Exception");
	}
	
	private static final long serialVersionUID = 1L;
}
