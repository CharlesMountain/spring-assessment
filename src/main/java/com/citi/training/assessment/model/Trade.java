package com.citi.training.assessment.model;

/**
* <h2> The Trade Class </h2>
* 
* Model of a trade which is represented
* as a row in Dao
*/
public class Trade {

    private int id;
    private String stock;
    private double price;
    private int volume;

    public Trade(int id, String stock, double price, int volume) {
        this.id = id;
        this.stock = stock;
        this.price = price;
        this.volume = volume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	@Override
    public String toString() {
        return "Trade [id=" + id + ", stock=" + stock +
               ", price=" + price + ", volume=" + volume + "]";
    }

}
