package com.citi.training.assessment.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.assessment.model.Trade;
import com.citi.training.assessment.service.TradeService;
import com.citi.training.assessment.rest.TradeController;

/**
* <h2> The TradeController Class </h2>
* 
* Handles all HTML requests of all methods
* Forwards requests to the service
*/
@RestController
@RequestMapping(TradeController.BASE_PATH)
public class TradeController {
	
	@Autowired
	TradeService tradeService;
	
	public static final String BASE_PATH = "/trade/";
	
	private static final Logger logger = LoggerFactory.getLogger(TradeController.class);
	
	public TradeController() {
		logger.debug("Trade Controller Initialized");
	}

	@RequestMapping(value="/", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Trade> findAll() {
		logger.debug("Forwarding GET method for function findAll to TradeService");
		return tradeService.findAll();
	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public void create(@RequestBody Trade trade) {
		logger.debug("Forwarding POST method for function create to TradeService");
		tradeService.create(trade);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Trade findById(@PathVariable int id) {
		logger.debug("Forwarding GET method for function findById to TradeService");
		return tradeService.findById(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable int id) {
		logger.debug("Forwarding DELETE method for function deleteById to TradeService");
		tradeService.deleteById(id);
	}
}
