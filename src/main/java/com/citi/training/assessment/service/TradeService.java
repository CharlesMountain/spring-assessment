package com.citi.training.assessment.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;

@Component
public class TradeService {

	@Autowired
	TradeDao tradeDao;
	
	private static final Logger logger = LoggerFactory.getLogger(TradeService.class);
	
	public TradeService() {
		logger.debug("Trade Service Initialized");
	}
	
	public List<Trade> findAll(){
		logger.debug("Forwarding method for function findAll to TradeDao");
		return tradeDao.findAll();
	}
	
	public Trade findById(int id) {
		logger.debug("Forwarding method for function findById to TradeDao");
		return tradeDao.findById(id);
	}
	
	public int create(Trade trade) {
		logger.debug("Forwarding method for function create to TradeDao");
		return tradeDao.create(trade);
	}
	
	public void deleteById(int id) {
		logger.debug("Forwarding method for function deleteById to TradeDao");
		tradeDao.deleteById(id);
	}
}
