package com.citi.training.assessment.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.assessment.dao.mysql.MysqlTradeDao;
import com.citi.training.assessment.exception.ResourceNotFoundException;
import com.citi.training.assessment.model.Trade;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlTradeDaoTests {

    @SpyBean
    JdbcTemplate tpl;

    @Autowired
    MysqlTradeDao mysqlTradeDao;

    @Test
    public void test_createAndFindAll_works() {
        mysqlTradeDao.create(new Trade(1, "F", 10.24,100));
        assertThat(mysqlTradeDao.findAll().size(), equalTo(1));
    }

    @Test
    public void test_createAndFindById_works() {
        int newId = mysqlTradeDao.create(
                            new Trade(2, "GM", 39.48,25));
        assertNotNull(mysqlTradeDao.findById(newId));
    }

    @Test(expected=ResourceNotFoundException.class)
    public void test_createAndFindById_throwsNotFound() {
        mysqlTradeDao.create(new Trade(1, "TSLA", 259.03,250));
        mysqlTradeDao.findById(99);
    }
}
