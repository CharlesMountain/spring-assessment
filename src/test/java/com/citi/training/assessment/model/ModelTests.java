package com.citi.training.assessment.model;

import org.junit.Test;

public class ModelTests {

    @Test
    public void test_Trade_fullConstructor() {
        Trade testTrade = new Trade(0,"AAPL", 204.23,20);
        assert(testTrade.getId() == 0);
        assert(testTrade.getStock().equals("AAPL"));
        assert(testTrade.getPrice() == 204.23);
        assert(testTrade.getVolume() == 20);
    }

    @Test
    public void test_Trade_setters() {
        Trade testTrade = new Trade(0, "MSFT", 137.30,50);
        testTrade.setId(1);
        testTrade.setStock("UBER");
        testTrade.setPrice(43.43);
        testTrade.setVolume(75);

        assert(testTrade.getId() == 1);
        assert(testTrade.getStock().equals("UBER"));
        assert(testTrade.getPrice() == 43.43);
        assert(testTrade.getVolume() == 75);
    }

    @Test
    public void test_Trade_toString() {
        Trade testTrade = new Trade(0, "AAPL", 204.23,20);

        assert(testTrade.toString() != null);
        assert(testTrade.toString().contains(testTrade.getStock()));
    }
}
