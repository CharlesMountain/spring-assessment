package com.citi.training.assessment.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeServiceTests {

	@Autowired
	TradeService tradeService;
	
	@MockBean
	private TradeDao mockTradeDao;
	
	@Test
	public void test_createRuns() {
		int newId = 1;
		
		when(mockTradeDao.create(any(Trade.class))).thenReturn(newId);
		int createdId = tradeService.create(new Trade(newId,"LYFT",67.51,20));
		
		
		assertEquals(newId, createdId);
	}
	
	@Test
	public void test_deleteByIdRuns() {
		int delId = 2;
		
		tradeService.deleteById(delId);
		
		verify(mockTradeDao, times(1)).deleteById(2);
	}
	
	@Test
	public void test_findByIdRuns() {
		int findId = 2;
		Trade findTrade = new Trade(findId,"GE",10.08,15);

		when(mockTradeDao.findById(findId)).thenReturn(findTrade);
		Trade foundTrade = tradeService.findById(findId);
		
		verify(mockTradeDao).findById(findId);
		assertEquals(findTrade,foundTrade);
	}
	
	@Test
	public void test_findAllRuns() {
		List<Trade> testList = new ArrayList<Trade>();
		testList.add(new Trade(11,"T",32.82,75));
		
		when(mockTradeDao.findAll()).thenReturn(testList);
		List<Trade> returnedList = tradeService.findAll();
	
		verify(mockTradeDao, times(1)).findAll();
		assertEquals(testList, returnedList);
	}

}
